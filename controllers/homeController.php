<?php
class homeController extends Controller {

    public function __construct(){
        parent::__construct();

        $user = new Usuarios();

        if(!$user->isLogged()){
            header("Location: ".BASE_URL."login");
        }
    }

    public function index() {
        $dados = array(
            'first_name' => '',
            'last_name' => ''
        );

        $user = new Usuarios($_SESSION['twlg']);
        $dados['first_name'] = $user->getNome();
        $dados['last_name'] = $user->getSobrenome();

        $this->loadTemplate('home', $dados);
    }
}